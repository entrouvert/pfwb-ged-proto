#! /usr/bin/env python

import sys
import os
import subprocess
from pysolr import Solr

conn = Solr('http://127.0.0.1:8080/solr/')
query = unicode(sys.argv[1], 'utf-8')

results = conn.search(query, rows=20, fl='* score')

print 'number of results:', results.hits
print 'qtime:', results.qtime

for result in results:
    print '%.2f - %s' % (result['score'], result['id'])

